import AVFoundation
import UIKit

enum StreamLink: String {
    case master = "https://devstreaming-cdn.apple.com/videos/tutorials/20190910/201gkmn78ytrxz/whats_new_in_sharing/hls_vod_mvp.m3u8"
    case variant = "https://devstreaming-cdn.apple.com/videos/tutorials/20190910/201gkmn78ytrxz/whats_new_in_sharing/0640/0640.m3u8"
}

class ViewController: UIViewController {
    @IBOutlet private var stateLabel: UILabel!
    private lazy var session: AVAssetDownloadURLSession = {
        let config = URLSessionConfiguration.background(withIdentifier: "background_identifier")
        let session = AVAssetDownloadURLSession(configuration: config, assetDownloadDelegate: self, delegateQueue: .main)
        
        return session
    }()

    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
    }
    

    // MARK: - Private functions
    
    @IBAction private func start() {
        guard let task = createTask(link: .variant) else {
            stateLabel.text = "### TASK IS NIL"
            return
        }
        session.getAllTasks(completionHandler: { tasks in
            print("### Number of tasks: \(tasks.count) \nStates: \(tasks.reduce("", { $0 + " " + "\($1.state.rawValue)" }))")
        })
        task.resume()
    }
    
    private func createTask(link: StreamLink) -> AVAssetDownloadTask? {
        guard let url = URL(string: link.rawValue) else {
            stateLabel.text = "### INVALID URL"
            return nil
        }
        let urlAsset = AVURLAsset(url: url)
        
        return session.makeAssetDownloadTask(asset: urlAsset,
                                             assetTitle: "Test Stream",
                                             assetArtworkData: nil,
                                             options: nil)
    }
}

// MARK: - AVAssetDownloadDelegate
extension ViewController: AVAssetDownloadDelegate {
    func urlSession(_ session: URLSession, assetDownloadTask: AVAssetDownloadTask, didFinishDownloadingTo location: URL) {
        stateLabel.text = "### DidFinishDownloadingTo \n\(location), state: \(assetDownloadTask.state.rawValue)"
    }
    
    func urlSession(_ session: URLSession, assetDownloadTask: AVAssetDownloadTask, didLoad timeRange: CMTimeRange, totalTimeRangesLoaded loadedTimeRanges: [NSValue], timeRangeExpectedToLoad: CMTimeRange) {
        let progress = loadedTimeRanges.reduce(0) { $0 + $1.timeRangeValue.duration.seconds / timeRangeExpectedToLoad.duration.seconds }
        stateLabel.text = "### Progress \n\(progress)"
    }
    
    func urlSession(_ session: URLSession, task: URLSessionTask, didCompleteWithError error: Error?) {
        stateLabel.text = "### DidCompleteWithError \n\(String(describing: error))"
    }

    func urlSession(_ session: URLSession, task: URLSessionTask, willBeginDelayedRequest request: URLRequest, completionHandler: @escaping (URLSession.DelayedRequestDisposition, URLRequest?) -> Void) {
        stateLabel.text = "### WillBeginDelayedRequest"
    }
    
    func urlSession(_ session: URLSession, taskIsWaitingForConnectivity task: URLSessionTask) {
        stateLabel.text = "### TaskIsWaitingForConnectivity"
    }
    
    func urlSession(_ session: URLSession, task: URLSessionTask, willPerformHTTPRedirection response: HTTPURLResponse, newRequest request: URLRequest, completionHandler: @escaping (URLRequest?) -> Void) {
        stateLabel.text = "### WillPerformHTTPRedirection"
    }
    
    func urlSession(_ session: URLSession, didReceive challenge: URLAuthenticationChallenge, completionHandler: @escaping (URLSession.AuthChallengeDisposition, URLCredential?) -> Void) {
        stateLabel.text = "### DidReceive challenge no task"
    }
    
    func urlSession(_ session: URLSession, task: URLSessionTask, needNewBodyStream completionHandler: @escaping (InputStream?) -> Void) {
        stateLabel.text = "### NeedNewBodyStream"
    }

    func urlSession(_ session: URLSession, didBecomeInvalidWithError error: Error?) {
        stateLabel.text = "### DidBecomeInvalidWithError"
    }
    
    func urlSession(_ session: URLSession, task: URLSessionTask, didFinishCollecting metrics: URLSessionTaskMetrics) {
        stateLabel.text = "### DidFinishCollecting"
    }

    func urlSession(_ session: URLSession, task: URLSessionTask, didSendBodyData bytesSent: Int64, totalBytesSent: Int64, totalBytesExpectedToSend: Int64) {
        stateLabel.text = "### DidSendBodyData totalBytesExpectedToSend"
    }
    
    func urlSession(_ session: URLSession, aggregateAssetDownloadTask: AVAggregateAssetDownloadTask, willDownloadTo location: URL) {
        stateLabel.text = "### AggregateAssetDownloadTask location"
    }
    
    func urlSession(_ session: URLSession, assetDownloadTask: AVAssetDownloadTask, didResolve resolvedMediaSelection: AVMediaSelection) {
        stateLabel.text = "### ResolvedMediaSelection"
    }
    
    func urlSession(_ session: URLSession, aggregateAssetDownloadTask: AVAggregateAssetDownloadTask, didCompleteFor mediaSelection: AVMediaSelection) {
        stateLabel.text = "### AggregateAssetDownloadTask mediaSelection"
    }
    
    func urlSession(_ session: URLSession, task: URLSessionTask, didReceive challenge: URLAuthenticationChallenge, completionHandler: @escaping (URLSession.AuthChallengeDisposition, URLCredential?) -> Void) {
        stateLabel.text = "### DidReceive challenge with task"
    }
    
    func urlSession(_ session: URLSession, aggregateAssetDownloadTask: AVAggregateAssetDownloadTask, didLoad timeRange: CMTimeRange, totalTimeRangesLoaded loadedTimeRanges: [NSValue], timeRangeExpectedToLoad: CMTimeRange, for mediaSelection: AVMediaSelection) {
        stateLabel.text = "### AggregateAssetDownloadTask totalTimeRangesLoaded"
    }
}
